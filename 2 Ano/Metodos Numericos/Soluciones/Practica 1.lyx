#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\begin_modules
multicol
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Enumerate
En cada caso determinar si la sucesión 
\begin_inset Formula $\left\{ a_{n}\right\} $
\end_inset

 converge o diverge y en caso de ser convergente hallar su límite.
\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Flex Multiple Columns
status open

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\frac{1}{n^{\alpha}}$
\end_inset

, 
\begin_inset Formula $\alpha>0$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\frac{n-1}{n}-\frac{n}{n-1}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\frac{3n^{2}-n+4}{2n^{2}+1}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\cos\frac{n\pi}{2}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\frac{n!}{n^{n}}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\frac{n^{p}}{e^{n}}$
\end_inset

, 
\begin_inset Formula $p>0$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $a_{n}=\sqrt[n]{n}$
\end_inset

.
\end_layout

\end_inset


\end_layout

\begin_layout Paragraph
Soluciones
\end_layout

\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Veamos que la sucesión es monótona decreciente:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{alignat*}{1}
 & 1\leq n\leq n+1\\
\iff & \left\langle x^{\alpha}\text{ estrictamente creciente }\left(\alpha>0\right)\right\rangle \\
 & 1^{\alpha}\leq n^{\alpha}\leq\left(n+1\right)^{\alpha}\\
\iff & \left\langle \text{\ensuremath{\nicefrac{1}{x}}}\text{ estrictamente decreciente }\left(x>0\right)\right\rangle \\
 & \frac{1}{1^{\alpha}}\geq\underbrace{\frac{1}{n^{\alpha}}}_{a_{n}}\geq\underbrace{\frac{1}{\left(n+1\right)^{\alpha}}}_{a_{n+1}}
\end{alignat*}

\end_inset


\end_layout

\begin_layout Itemize
Veamos que la sucesión es acotada inferiormente por 
\begin_inset Formula $0$
\end_inset

.
 Supongamos existe 
\begin_inset Formula $1\leq n$
\end_inset

 tal que 
\begin_inset Formula $\frac{1}{n^{\alpha}}<0$
\end_inset

, luego:
\begin_inset Formula 
\begin{alignat*}{1}
 & \frac{1}{n^{\alpha}}<0\\
\iff & \left\langle \text{sgn}\left(x\right)=\text{sgn}\left(x^{-1}\right)\right\rangle \\
 & n^{\alpha}<0\\
\iff & \left\langle \sqrt[\alpha]{x}\text{ estrictamente creciente }\left(\alpha,x>0\right)\right\rangle \\
 & n=\sqrt[\alpha]{n^{\alpha}}<\sqrt[\alpha]{0}=0\\
\iff & \left\langle 1\leq n\right\rangle \\
 & 1\leq0
\end{alignat*}

\end_inset


\end_layout

\begin_layout Itemize
Puesto que 
\begin_inset Formula $a_{n}$
\end_inset

 es monótona decreciente y acotada inferiormente, entonces converge.
\end_layout

\end_deeper
\begin_layout Enumerate
Observemos que:
\begin_inset Formula 
\begin{alignat*}{1}
 & a_{n}\\
= & \left\langle \text{definicion}\right\rangle \\
 & \frac{n-1}{n}-\frac{n}{n-1}\\
= & \left\langle \text{suma de fracciones}\right\rangle \\
 & \frac{n}{n}-\frac{1}{n}-\frac{n}{n-1}\\
= & \left\langle \text{factor comun }n\right\rangle \\
 & \frac{n}{n}-\frac{1}{n}-\frac{n}{n\left(1-\frac{1}{n}\right)}\\
= & \left\langle n\geq1\right\rangle \\
 & 1-\frac{1}{n}-\frac{1}{1-\frac{1}{n}}
\end{alignat*}

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Sea 
\begin_inset Formula $f\left(x\right)=1-\frac{1}{x}-\frac{1}{1-\frac{1}{x}}$
\end_inset

 luego
\begin_inset Formula 
\[
\lim_{x\to\infty}1-\overbrace{\frac{1}{x}}^{\to0}-\overbrace{\frac{1}{1-\underbrace{\frac{1}{x}}_{\to0}}}^{\to1}=1-0-1=1
\]

\end_inset

por lo que la sucesión converge a 
\begin_inset Formula $1$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Enumerate
Sean 
\begin_inset Formula $f\left(x\right)=3x^{2}-x+4$
\end_inset

 y 
\begin_inset Formula $g\left(x\right)=2x^{2}+1$
\end_inset

.
 Observemos que:
\end_layout

\begin_deeper
\begin_layout Itemize
\begin_inset Formula $f'\left(x\right)=6x-1$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $g'\left(x\right)=4x$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\lim_{x\to\infty}\frac{f'\left(x\right)}{g'\left(x\right)}=\lim_{x\to\infty}\frac{6x-1}{4x}=\lim_{x\to\infty}\overbrace{\frac{6x}{4x}}^{\to\nicefrac{3}{2}}-\frac{1}{4x}=$
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Formula $=\frac{3}{2}-\lim_{x\to\infty}\overbrace{\frac{1}{4x}}^{\to0}=\frac{3}{2}$
\end_inset

.
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\end_deeper
\begin_layout Standard
Luego como 
\begin_inset Formula $\lim_{x\to\infty}\frac{\overbrace{f\left(x\right)}^{\to\infty}}{\underbrace{g\left(x\right)}_{\to\infty}}$
\end_inset

 por regla de L'Hopial resulta:
\begin_inset Formula 
\[
\lim_{x\to\infty}\frac{f\left(x\right)}{g\left(x\right)}=\lim_{x\to\infty}\frac{f'\left(x\right)}{g'\left(x\right)}=\frac{3}{2}
\]

\end_inset

 por lo que 
\begin_inset Formula $a_{n}$
\end_inset

 converge a 
\begin_inset Formula $\frac{3}{2}$
\end_inset

.
\end_layout

\end_deeper
\begin_layout Enumerate
Sean 
\begin_inset Formula $b_{n}$
\end_inset

 la subsucesión de los números páres y 
\begin_inset Formula $c_{n}$
\end_inset

 la subsucesión de los números impares.
 Tenemos que:
\end_layout

\begin_deeper
\begin_layout Itemize
\begin_inset Formula $\lim_{n\to\infty}b_{n}=-1$
\end_inset

.
\end_layout

\begin_layout Itemize
\begin_inset Formula $\lim_{n\to\infty}c_{n}=0$
\end_inset

.
\end_layout

\begin_layout Standard
Puesto que una sucesión es convergente si y sólo si todas sus subsucesiones
 convergen al mimsmo límite, podemos concluir que 
\begin_inset Formula $a_{n}$
\end_inset

 no es convergente.
\end_layout

\end_deeper
\begin_layout Enumerate
Sean 
\begin_inset Formula $l_{n}=0$
\end_inset

 y 
\begin_inset Formula $r_{n}=\frac{1}{n}$
\end_inset

.
 Observemos que:
\begin_inset Formula 
\[
l_{n}=0\leq\frac{n!}{n^{n}}=\frac{n}{n}\cdot\frac{n-1}{n}\cdot\dots\cdot\frac{2}{n}\cdot\frac{1}{n}\leq1\cdot1\cdot\dots\cdot1\cdot\frac{1}{n}=r_{n}
\]

\end_inset

puesto que cada factor de la expresión es positivo (por ser cociente de
 numeros positivos) y que cada factor es menor o igual a uno.
\end_layout

\begin_deeper
\begin_layout Standard
Resulta fácil ver que 
\begin_inset Formula $l_{n}$
\end_inset

 y 
\begin_inset Formula $r_{n}$
\end_inset

 convergen a 0 y por teorema del sandwitch 
\begin_inset Formula $a_{n}$
\end_inset

 también lo hace.
\end_layout

\end_deeper
\begin_layout Enumerate
Sea 
\begin_inset Formula $f\left(x\right)=\frac{x^{p}}{e^{x}}$
\end_inset

 luego 
\begin_inset Formula $f\left(x\right)=e^{\ln\left(\frac{x^{p}}{e^{x}}\right)}=e^{\ln\left(x^{p}\right)-\ln\left(e^{x}\right)}=e^{p\ln\left(x\right)-x}$
\end_inset

.
 Observemos que:
\begin_inset Formula 
\[
\lim_{x\to\infty}\left(\frac{p\ln\left(x\right)}{x}-1\right)=\lim_{x\to\infty}\frac{p\ln\left(x\right)}{x}-\lim_{x\to\infty}1=\lim_{x\to\infty}\frac{p\ln\left(x\right)}{x}-1=
\]

\end_inset


\begin_inset Formula 
\[
=p\cdot\lim_{x\to\infty}\frac{\overbrace{\ln\left(x\right)}^{\to\infty}}{\underbrace{x}_{\to\infty}}-1\overset{LH}{=}p\cdot\lim_{x\to\infty}\frac{\nicefrac{1}{x}}{1}-1=p\cdot0-1=-1
\]

\end_inset


\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Ahora:
\begin_inset Formula 
\[
\lim_{x\to\infty}p\ln\left(x\right)-x=\lim_{x\to\infty}\left(p\ln\left(x\right)-x\right)\cdot\frac{x}{x}=
\]

\end_inset


\begin_inset Formula 
\[
=\lim_{x\to\infty}\frac{p\ln\left(x\right)-x}{x}\cdot x=\lim_{x\to\infty}\underbrace{\left(\frac{p\ln\left(x\right)}{x}-1\right)}_{\to-1}\cdot\underbrace{x}_{\to\infty}=-\infty
\]

\end_inset


\end_layout

\begin_layout Standard
y finalmente 
\begin_inset Formula $\lim_{x\to\infty}f\left(x\right)=\lim_{x\to\infty}e^{\overbrace{p\ln\left(x\right)-x}^{\to-\infty}}=0$
\end_inset

 por lo que 
\begin_inset Formula $a_{n}$
\end_inset

 converge a 0.
\end_layout

\end_deeper
\begin_layout Enumerate
Sea 
\begin_inset Formula $f\left(x\right)=\sqrt[n]{n}=n^{\nicefrac{1}{n}}=e^{\ln\left(n^{\nicefrac{1}{n}}\right)}=e^{\frac{\ln\left(n\right)}{n}}$
\end_inset

, luego:
\begin_inset Formula 
\[
\lim_{x\to\infty}\frac{\overbrace{\ln\left(n\right)}^{\to\infty}}{\underbrace{n}_{\to\infty}}\overset{LH}{=}\lim_{x\to\infty}\frac{1}{x}=0
\]

\end_inset

por lo que 
\begin_inset Formula $\lim_{x\to\infty}f\left(x\right)=e^{0}=1$
\end_inset

; luego la sucesión converge a 1.
\end_layout

\end_deeper
\begin_layout Enumerate
En cada caso determinar si la serie converge o diverge y en caso de ser
 convergente hallar su límite.
\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Flex Multiple Columns
status open

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{n\left(n+1\right)}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{n\left(n+2\right)}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{n}{\sqrt{n^{2}+1}}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\left(-\frac{1}{2}\right)^{n}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}3\left(\frac{3}{2}\right)^{n}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{2^{n}+1}{2^{n+1}}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{\left(2n+1\right)\left(2n+3\right)}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\left(\frac{1}{2^{n}}-\frac{1}{3^{n}}\right)$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{n!}{2^{n}}$
\end_inset

.
\end_layout

\end_inset


\end_layout

\begin_layout Paragraph
Soluciones
\end_layout

\begin_layout Enumerate
Sea 
\begin_inset Formula $b_{n}=\frac{1}{n}$
\end_inset

, observemos que 
\begin_inset Formula $b_{n}-b_{n+1}=\frac{1}{n}-\frac{1}{n+1}=\frac{\left(n+1\right)-n}{n\left(n+1\right)}=\frac{1}{n\left(n+1\right)}$
\end_inset

; luego podemos reescribir a la serie como una serie telescópica:
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\frac{1}{n\left(n+1\right)}=\sum_{n=1}^{\infty}\left(b_{n}-b_{n+1}\right)
\]

\end_inset

Calculemos ahora el límite de 
\begin_inset Formula $b_{n}$
\end_inset

:
\begin_inset Formula 
\[
\lim_{n\to\infty}b_{n}=\lim_{n\to\infty}\frac{1}{\underbrace{n}_{\to\infty}}=0
\]

\end_inset

Podemos concluir entonces que la serie converge a 
\begin_inset Formula $b_{1}-0=1$
\end_inset

.
\end_layout

\begin_layout Enumerate
Sea 
\begin_inset Formula $b_{n}=\frac{1}{n}$
\end_inset

, observemos que 
\begin_inset Formula $b_{n}-b_{n+2}=\frac{2}{n\left(n+2\right)}$
\end_inset

 y además:
\begin_inset Formula 
\[
b_{n}-b_{n+2}=\frac{1}{n}-\frac{1}{n+2}=\frac{1}{n}-\frac{1}{n+2}+\frac{1}{n+1}-\frac{1}{n+1}=
\]

\end_inset


\begin_inset Formula 
\[
=b_{n}-b_{n+2}+b_{n+1}-b_{n+1}=b_{n}+b_{n+1}-\left(b_{n+1}+b_{n+2}\right)
\]

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Sea entonces 
\begin_inset Formula $c_{n}=b_{n}+b_{n+1}$
\end_inset

 podemos reescribir a la serie original como:
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\frac{1}{n\left(n+2\right)}=\sum_{n=1}^{\infty}\frac{1}{2}\left(b_{n}-b_{n+2}\right)=\sum_{n=1}^{\infty}\frac{1}{2}\left(c_{n}-c_{n+1}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Como 
\begin_inset Formula $\lim_{n\to\infty}b_{n}=\lim_{n\to\infty}\frac{1}{n}=0=\lim_{n\to\infty}\frac{1}{n+1}=\lim_{n\to\infty}b_{n+1}$
\end_inset

 podemos concluir que:
\begin_inset Formula 
\[
\lim_{n\to\infty}c_{n}=\lim_{n\to\infty}\left(b_{n}+b_{n+1}\right)=0+0=0
\]

\end_inset

Por propiedad de linealidad y propiedad telescópica la serie converge a:
 
\begin_inset Formula 
\[
\frac{1}{2}\left(c_{1}-0\right)=\frac{1}{2}\left(b_{1}+b_{2}\right)=\frac{1}{2}\left(1+\frac{1}{2}\right)=\frac{1}{2}\cdot\frac{3}{2}=\frac{3}{4}
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
Observemos que 
\begin_inset Formula 
\[
\lim_{n\to\infty}\frac{n}{\sqrt{n^{2}+1}}=\lim_{n\to\infty}\frac{n}{\sqrt{n^{2}+1}}\cdot\frac{\nicefrac{1}{n}}{\nicefrac{1}{n}}=\lim_{n\to\infty}\frac{1}{\frac{\sqrt{n^{2}+1}}{n}}=
\]

\end_inset


\begin_inset Formula 
\[
=\lim_{n\to\infty}\frac{1}{\frac{\sqrt{n^{2}+1}}{\sqrt{n^{2}}}}=\lim_{n\to\infty}\frac{1}{\sqrt{\frac{n^{2}+1}{n^{2}}}}=\lim_{n\to\infty}\frac{1}{\sqrt{\frac{n^{2}}{n^{2}}+\frac{1}{n^{2}}}}=
\]

\end_inset


\begin_inset Formula 
\[
=\lim_{n\to\infty}\frac{1}{\sqrt{1+\underbrace{\frac{1}{n^{2}}}_{\to0}}}=1\neq0
\]

\end_inset

luego por el teorema de condicion necesaria para la convergencia, la serie
 diverge.
\end_layout

\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Convergencia: Observemos que
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\left(-\frac{1}{2}\right)^{n}=\sum_{n=1}^{\infty}\left(-1\right)^{n}\left(\frac{1}{2}\right)^{n}=-\sum_{n=1}^{\infty}\left(-1\right)^{n-1}\left(\frac{1}{2}\right)^{n}
\]

\end_inset

Además 
\begin_inset Formula $\left\{ \left(\frac{1}{2}\right)^{n}\right\} $
\end_inset

 es monotona decreciente y como 
\begin_inset Formula $\lim_{n\to\infty}\left(\frac{1}{2}\right)^{n}=\lim_{n\to\infty}\frac{1}{\underbrace{2^{n}}_{\to\infty}}=0$
\end_inset

 por el criterio de Libniz la serie converge.
\end_layout

\begin_layout Itemize
Suma: Observemos que
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\left(-\frac{1}{2}\right)^{n}=\sum_{n=0}^{\infty}\left(-\frac{1}{2}\right)^{n+1}=\sum_{n=0}^{\infty}\left(-\frac{1}{2}\right)\left(-\frac{1}{2}\right)^{n}=-\frac{1}{2}\sum_{n=0}^{\infty}\left(-\frac{1}{2}\right)^{n}
\]

\end_inset

y como 
\begin_inset Formula $\sum_{n=0}^{\infty}\left(-\frac{1}{2}\right)^{n}$
\end_inset

 es una serie geométrica de razón menor a 1 resulta que la serie original
 converge a
\begin_inset Formula 
\[
-\frac{1}{2}\left(\frac{1}{1-\left(-\frac{1}{2}\right)}\right)=-\frac{1}{2}\left(\frac{1}{1+\frac{1}{2}}\right)=-\frac{1}{2}\left(\frac{1}{\nicefrac{3}{2}}\right)=-\frac{1}{2}\cdot\frac{2}{3}=-\frac{1}{3}
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
Observemos que
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}3\left(\frac{3}{2}\right)^{n}=3\sum_{n=1}^{\infty}\left(\frac{3}{2}\right)^{n}=3\sum_{n=0}^{\infty}\left(\frac{3}{2}\right)^{n+1}=3\sum_{n=0}^{\infty}\frac{3}{2}\left(\frac{3}{2}\right)^{n}=3\frac{3}{2}\sum_{n=0}^{\infty}\left(\frac{3}{2}\right)^{n}
\]

\end_inset

y por aparecer una serie geometrica de razón mayor a 1, la serie original
 no converge.
\end_layout

\begin_layout Enumerate
Observemos que
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\frac{2^{n}+1}{2^{n+1}}=\sum_{n=1}^{\infty}\frac{1}{2}\frac{2^{n}+1}{2^{n}}=\frac{1}{2}\sum_{n=1}^{\infty}\frac{2^{n}+1}{2^{n}}=\frac{1}{2}\sum_{n=1}^{\infty}\left(\frac{2^{n}}{2^{n}}+\frac{1}{2^{n}}\right)=
\]

\end_inset


\begin_inset Formula 
\[
=\frac{1}{2}\sum_{n=1}^{\infty}\left(1+\frac{1}{2^{n}}\right)
\]

\end_inset

luego como 
\begin_inset Formula $\lim_{n\to\infty}\left(1+\frac{1}{2^{n}}\right)=1\neq0$
\end_inset

 por el teorema de condición necesaria para la convergencia, la serie diverge.
\end_layout

\begin_layout Enumerate
Descomponemos en fracciones simples:
\begin_inset Formula 
\[
\frac{1}{\left(2n+1\right)\left(2n+3\right)}=\frac{A}{2n+1}+\frac{B}{2n+3}=\frac{A\left(2n+3\right)+B\left(2n+1\right)}{\left(2n+1\right)\left(2n+3\right)}
\]

\end_inset


\begin_inset Formula 
\[
n=-\frac{1}{2}\Rightarrow1=A\left(-1+3\right)\Rightarrow A=\frac{1}{2}
\]

\end_inset


\begin_inset Formula 
\[
n=-\frac{3}{2}\Rightarrow1=B\left(-3+1\right)\Rightarrow B=-\frac{1}{2}
\]

\end_inset

por lo tanto:
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\frac{1}{\left(2n+1\right)\left(2n+3\right)}=\sum_{n=1}^{\infty}\left(\frac{\nicefrac{1}{2}}{2n+1}-\frac{\nicefrac{1}{2}}{2n+3}\right)=\frac{1}{2}\sum_{n=1}^{\infty}\left(\frac{1}{2n+1}-\frac{1}{2n+3}\right)
\]

\end_inset


\end_layout

\begin_deeper
\begin_layout Standard
Sea entonces 
\begin_inset Formula $b_{n}=\frac{1}{2n+1}$
\end_inset

 podemos reescribir la serie como:
\begin_inset Formula 
\[
\frac{1}{2}\sum_{n=1}^{\infty}\left(b_{n}-b_{n+1}\right)
\]

\end_inset

y por propiedad telescopica la serie converge a
\begin_inset Formula 
\[
\frac{1}{2}\left(b_{1}-\lim_{n\to\infty}\frac{1}{2n+1}\right)=\frac{1}{2}\left(\frac{1}{3}-0\right)=\frac{1}{6}
\]

\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
Observemos que:
\begin_inset Formula 
\[
\sum_{n=1}^{\infty}\left(\frac{1}{2^{n}}-\frac{1}{3^{n}}\right)=\sum_{n=1}^{\infty}\frac{1}{2^{n}}-\sum_{n=1}^{\infty}\frac{1}{3^{n}}=\sum_{n=0}^{\infty}\frac{1}{2^{n+1}}-\sum_{n=0}^{\infty}\frac{1}{3^{n+1}}=
\]

\end_inset


\begin_inset Formula 
\[
=\sum_{n=0}^{\infty}\frac{1}{2^{n+1}}-\sum_{n=0}^{\infty}\frac{1}{3^{n+1}}=\sum_{n=0}^{\infty}\frac{1}{2^{n}}\cdot\frac{1}{2}-\sum_{n=0}^{\infty}\frac{1}{3^{n}}\cdot\frac{1}{3}=
\]

\end_inset


\begin_inset Formula 
\[
=\frac{1}{2}\sum_{n=0}^{\infty}\frac{1}{2^{n}}-\frac{1}{3}\sum_{n=0}^{\infty}\frac{1}{3^{n}}
\]

\end_inset

y por ser series geométricas de razón menor a uno la serie converge a
\begin_inset Formula 
\[
\frac{1}{2}\left(\frac{1}{1-\nicefrac{1}{2}}\right)-\frac{1}{3}\left(\frac{1}{1-\nicefrac{1}{3}}\right)=\frac{1}{2}\left(\frac{1}{\nicefrac{1}{2}}\right)-\frac{1}{3}\left(\frac{1}{\nicefrac{2}{3}}\right)=1-\frac{3}{6}=\frac{1}{2}
\]

\end_inset


\end_layout

\begin_layout Enumerate
Consideremos 
\begin_inset Formula $n\geq3$
\end_inset

, luego
\begin_inset Formula 
\[
\frac{n!}{2^{n}}=\frac{1\cdot2\cdot3\cdot\dots\cdot n}{2\cdot2\cdot2\cdot\dots\cdot\dot{2}}\geq\frac{1}{2}\cdot1\cdot\dots\cdot1=\frac{1}{2}
\]

\end_inset

por lo que 
\begin_inset Formula $\lim_{n\to\infty}\frac{n!}{2^{n}}\neq0$
\end_inset

 y la serie diverge el por teorema de condicion necesaria para la convergencia.
\end_layout

\end_deeper
\begin_layout Enumerate
Estudiar el carácter de las siguientes series en función de los valores
 posibles de los parámetros 
\begin_inset Formula $a$
\end_inset

 y 
\begin_inset Formula $b$
\end_inset

.
 En cada caso utilizar alguno de los criterios de convergencia para justificar
 la respuesta.
\end_layout

\begin_deeper
\begin_layout Standard
\begin_inset Flex Multiple Columns
status open

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{3^{n}+1}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{4^{n}}{3^{n}-1}$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{\left|b\right|^{n}}{n\left(1+a^{n}\right)}$
\end_inset

, 
\begin_inset Formula $a>1$
\end_inset

, 
\begin_inset Formula $\left|b\right|\neq a$
\end_inset

,
\end_layout

\begin_layout Enumerate
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{a^{n}}{\left(n+2\right)\left(n+a\right)5^{n}}$
\end_inset

, 
\begin_inset Formula $a>0$
\end_inset

.
\end_layout

\end_inset


\end_layout

\begin_layout Paragraph
Soluciones
\end_layout

\begin_layout Enumerate
Observemos que 
\begin_inset Formula $\frac{1}{3^{n}+1}>0$
\end_inset

 luego:
\begin_inset Formula 
\[
\lim_{x\to\infty}\frac{\frac{1}{3^{n+1}+1}}{\frac{1}{3^{n}+1}}=\lim_{x\to\infty}\frac{3^{n}+1}{3^{n+1}+1}\overset{LH}{=}\lim_{x\to\infty}\frac{n3^{n-1}}{\left(n+1\right)3^{n}}=\lim_{x\to\infty}\frac{n}{\left(n+1\right)3}\overset{LH}{=}
\]

\end_inset


\begin_inset Formula 
\[
\overset{LH}{=}\lim_{x\to\infty}\frac{1}{3}=\frac{1}{3}
\]

\end_inset

Como 
\begin_inset Formula $\frac{1}{3}<1$
\end_inset

, por el criterio del cociente la serie converge.
\end_layout

\begin_layout Enumerate
Observemos que 
\begin_inset Formula $0<\left(\frac{4}{3}\right)^{n}<\frac{4^{n}}{3^{n}-1}$
\end_inset

 y que 
\begin_inset Formula $\left(\frac{4}{3}\right)^{n}$
\end_inset

 diverge por ser una geométrica de razón mayor a 1.
\end_layout

\begin_deeper
\begin_layout Standard
Sean 
\begin_inset Formula $a_{n}=\left(\frac{4}{3}\right)^{n}$
\end_inset

 y 
\begin_inset Formula $b_{n}=\frac{4^{n}}{3^{n}-1}$
\end_inset

, como 
\begin_inset Formula $a_{n}\leq1\cdot b_{n}$
\end_inset

 y 
\begin_inset Formula $a_{n}$
\end_inset

 diverge, entonces por el criterio de comparación, la serie diverge.
\end_layout

\end_deeper
\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Consideremos 
\begin_inset Formula $\left|b\right|>a>1$
\end_inset

: La serie diverge.
 COMPLETAR.
\end_layout

\begin_layout Itemize
Consideremos 
\begin_inset Formula $\left|b\right|<a$
\end_inset

: Observemos que
\begin_inset Formula 
\[
\lim_{n\to\infty}\sqrt[n]{\frac{\left|b\right|^{n}}{a^{n}}}=\lim_{n\to\infty}\frac{\left|b\right|}{a}=\frac{\left|b\right|}{a}<1
\]

\end_inset

por lo que la serie 
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{\left|b\right|^{n}}{a^{n}}$
\end_inset

 converge como consecuencia del criterio de la raiz.
\end_layout

\begin_deeper
\begin_layout Standard
Sean 
\begin_inset Formula $a_{n}=\frac{\left|b\right|^{n}}{n\left(1+a^{n}\right)}$
\end_inset

 y 
\begin_inset Formula $b_{n}=\frac{\left|b\right|^{n}}{a^{n}}$
\end_inset

, como 
\begin_inset Formula $a_{n}\leq1\cdot b_{n}$
\end_inset

 y 
\begin_inset Formula $\sum_{n=1}^{\infty}b_{n}$
\end_inset

 converge, entonces por el criterio de comparación, la serie original converge.
 
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
\begin_inset space ~
\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Consideremos 
\begin_inset Formula $a\leq5$
\end_inset

: Observemos que 
\begin_inset Formula $\frac{1}{n^{2}}\leq1\cdot\frac{1}{2^{n}}$
\end_inset

 (para 
\begin_inset Formula $n\geq1$
\end_inset

) y que 
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{2^{n}}$
\end_inset

 converge (serie geométrica) luego por el criterio de comparación también
 converge 
\begin_inset Formula $\sum_{n=1}^{\infty}\frac{1}{n^{2}}$
\end_inset

.
\end_layout

\begin_deeper
\begin_layout Standard
Además como
\begin_inset Formula 
\[
\frac{a^{n}}{\left(n+2\right)\left(n+a\right)5^{n}}=\frac{1}{\left(n+2\right)\left(n+a\right)\left(\frac{5}{a}\right)^{n}}<\frac{1}{n^{2}}
\]

\end_inset

nuevamente por el criterio de comparación, la serie original converge.
\end_layout

\end_deeper
\begin_layout Itemize
Consideremos 
\begin_inset Formula $a>5$
\end_inset

: Observemos que
\begin_inset Formula 
\[
\lim_{n\to\infty}\frac{\frac{a^{n+1}}{\left(n+1+2\right)\left(n+1+a\right)5^{n+1}}}{\frac{a^{n}}{\left(n+2\right)\left(n+a\right)5^{n}}}=\lim_{n\to\infty}\frac{a^{n+1}\left(n+2\right)\left(n+a\right)5^{n}}{\left(n+1+2\right)\left(n+1+a\right)5^{n+1}a^{n}}=
\]

\end_inset


\begin_inset Formula 
\[
=\lim_{n\to\infty}\frac{a\left(n+2\right)\left(n+a\right)}{\left(n+3\right)\left(n+1+a\right)5}=\lim_{n\to\infty}\frac{a\left(n^{2}+na+2n+2a\right)}{5n^{2}+5n+5na+15n+15+15a}=
\]

\end_inset


\begin_inset Formula 
\[
=\lim_{n\to\infty}\frac{an^{2}+na^{2}+2n+2a}{5n^{2}+5n+5na+15n+15+15a}\overset{LH}{=}\lim_{n\to\infty}\frac{2an+a^{2}+2a}{10n+5+5a+15}\overset{LH}{=}
\]

\end_inset


\begin_inset Formula 
\[
\overset{LH}{=}\lim_{n\to\infty}\frac{2a}{10}=\lim_{n\to\infty}\frac{a}{5}>1
\]

\end_inset

luego por el criterio del cociente, la serie diverge.
\end_layout

\end_deeper
\end_deeper
\end_body
\end_document
